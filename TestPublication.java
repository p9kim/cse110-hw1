package edu.ucsd.cse110.library;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import edu.ucsd.cse110.library.rules.RuleObjectBasedLateFeesStrategy;

public class TestPublication {
	
	Publication pub;
	Member max;
	LibraryFacade libF;
	Library lib;
	
	@Before
	public void setUp() {
		pub = new Book("Lords of the Rings", new RuleObjectBasedLateFeesStrategy());
		max = new Member("Max", MemberType.Teacher);
		//libF = new libFraryFacade( "Max", "Lords of the Rings", new RuleObjectBasedLateFeesStrategy(), MemberType.Teacher );
		libF = new LibraryFacade( max, pub );
		lib = new Library(max, pub);
		
	}
	
	@Test
	public void testCheckout() throws ParseException {
		LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
		pub.checkout(max,checkoutDate);
		assertTrue(pub.isCheckout());
		assertEquals(max, pub.getMember());
		assertEquals(checkoutDate, pub.getCheckoutDate());
	}
	
	@Test
	public void testReturnOk() throws ParseException {
		LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
		LocalDate returnDate  = LocalDate.of(20014, 12, 3);
		pub.checkout(max,checkoutDate);
		pub.pubReturn(returnDate);
		assertFalse(pub.isCheckout());
		assertEquals(0.00, max.getDueFees(),0.01);
	}
	
	@Test
	public void testReturnLate() throws ParseException {
		LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
		LocalDate returnDate  = LocalDate.of(20014, 12, 20);
		pub.checkout(max,checkoutDate);
		pub.pubReturn(returnDate);
		assertFalse(pub.isCheckout());
		assertEquals(5.00, max.getDueFees(),0.01);
	}
	
	@Test
	public void testcheckoutPublication() throws ParseException{
		LocalDate checkoutDate = LocalDate.of(20014, 12,  1);
		libF.setCDate(checkoutDate);
		libF.checkoutPublication( libF.member, libF.book );
		assertTrue( libF.book.isCheckout());
		assertEquals( libF.member, libF.book.getMember());
		assertEquals( checkoutDate, libF.getCDate());
	}
	
	@Test
	public void testreturnPublication() throws ParseException{
		LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
		LocalDate returnDate = LocalDate.of(20014, 12, 3);
		libF.setCDate(checkoutDate);
		libF.setRDate(returnDate);
		libF.checkoutPublication(libF.member, libF.book );
		libF.returnPublication(libF.book);
		assertFalse(libF.book.isCheckout());
		assertEquals(0.00, libF.getFee(libF.member), 0.01);
		assertTrue(libF.hasFee(libF.member));
			
	}
	
	@Test
	public void testreturnPublicationLate() throws ParseException{
		LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
		LocalDate returnDate  = LocalDate.of(20014, 12, 20);
		libF.setCDate(checkoutDate);
		libF.setRDate(returnDate);
		libF.checkoutPublication(libF.member, libF.book);
		libF.returnPublication(libF.book);
		assertFalse(libF.book.isCheckout());
		assertEquals(5.00, libF.getFee(libF.member), 0.01);
		assertFalse(libF.hasFee(libF.member));
		
	}
	
	@Test
	public void testLibcheckoutPublication() throws ParseException{
		lib.createFacade();
		LocalDate checkoutDate = LocalDate.of(20014, 12,  1);
		lib.facade.setCDate(checkoutDate);
		lib.facade.checkoutPublication( lib.member, lib.book );
		assertTrue( lib.book.isCheckout());
		assertEquals( lib.facade.member, lib.facade.book.getMember());
		assertEquals( checkoutDate, lib.facade.getCDate());
	}
	
	@Test
	public void testLibreturnPublication() throws ParseException{
		lib.createFacade();
		LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
		LocalDate returnDate = LocalDate.of(20014, 12, 3);
		lib.facade.setCDate(checkoutDate);
		lib.facade.setRDate(returnDate);
		lib.facade.checkoutPublication(lib.member, lib.book );
		lib.facade.returnPublication(lib.book);
		assertFalse(lib.book.isCheckout());
		assertEquals(0.00, lib.facade.getFee(lib.member), 0.01);
		assertTrue(lib.facade.hasFee(lib.member));
			
	}
	
	@Test
	public void testLibreturnPublicationLate() throws ParseException{
		lib.createFacade();
		LocalDate checkoutDate = LocalDate.of(20014, 12, 1);
		LocalDate returnDate  = LocalDate.of(20014, 12, 20);
		lib.facade.setCDate(checkoutDate);
		lib.facade.setRDate(returnDate);
		lib.facade.checkoutPublication(lib.member, lib.book);
		lib.facade.returnPublication(lib.book);
		assertFalse(lib.book.isCheckout());
		assertEquals(5.00, lib.facade.getFee(lib.member), 0.01);
		assertFalse(lib.facade.hasFee(lib.member));
		
	}
	
	
	
	
}
