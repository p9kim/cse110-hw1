package edu.ucsd.cse110.library.rules;

import java.text.DecimalFormat;

public class ChargePerDayFeeWithMaxFeeAction implements Action {
	private double perDayFee;
	private int freeDays;
	private double maxAssessment;
	private String errorMessage;
	
	public ChargePerDayFeeWithMaxFeeAction(double perDayFee, int freeDays, double maxAssessment) {
		this.perDayFee = perDayFee;
		this.freeDays = freeDays;
		this.maxAssessment = maxAssessment;
		this.errorMessage = null;
	}

	@Override
	public void execute(Properties prop) {
		double assessed = (prop.getDays()-freeDays)*perDayFee;
		if (prop.getCurrentFee()+assessed>maxAssessment)
			if (prop.getCurrentFee()<maxAssessment)
				assessed=maxAssessment-prop.getCurrentFee();
			else
				assessed=0;	
		prop.getMember().applyLateFee(assessed);
		if(prop.getMember().getDueFees()>maxAssessment) {
			DecimalFormat df = new DecimalFormat("#.##");
			errorMessage= prop.getMember().getName() + " account should never be charged more than $" + 
					df.format(maxAssessment) + " but is currently charged $" +
					df.format(prop.member.getDueFees()) + ".\n";			
		}
	}

	@Override
	public String getErrors() {
		return errorMessage;
	}

}
