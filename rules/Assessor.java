package edu.ucsd.cse110.library.rules;


public interface Assessor {
	public boolean evaluate(Properties prop);
	public String getErrors();
}
