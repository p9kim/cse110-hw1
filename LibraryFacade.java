package edu.ucsd.cse110.library;

import java.time.LocalDate;

public class LibraryFacade {
	
	//Publication pub;
	Publication book;
	Member member;
	private LocalDate cDate;
	private LocalDate rDate;
	
	/*String memName, String bookTitle, LateFeesStrategy fee, MemberType memType,*/
	public LibraryFacade( Member mem, Publication b){
		
		//member = new Member( memName, memType );
		//book = new Book( bookTitle, fee );
		this.member = mem;
		this.book = b;
		//pub = new Publication( fee );
	}
	
	public void checkoutPublication( Member mem, Publication p ){
		
		if( p.isCheckout() == false )
			p.checkout(mem, cDate);
		
	}
	
	
	public void returnPublication( Publication p ){
		p.pubReturn( rDate );
	}
	
	public double getFee( Member mem ) {
		
		return mem.getDueFees();
	}
	
	public boolean hasFee( Member mem ){
		if( mem.getDueFees() == 0.00 )
			return true;
		else
			return false;
	}
	
	public LocalDate getCDate(){
		return cDate;
	}
	
	public void setCDate( LocalDate cd){
		cDate = cd;
	}
	
	public LocalDate getRDate(){
		return rDate;
	}
	
	public void setRDate( LocalDate rd){
		rDate = rd;
	}
}
