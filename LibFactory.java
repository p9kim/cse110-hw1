package edu.ucsd.cse110.library;

public class LibFactory {
	public static LibraryFacade createLib(Library lib){
		return new LibraryFacade(lib.member, lib.book);
	}
}
