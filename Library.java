package edu.ucsd.cse110.library;

public class Library {
	Publication book;
	Member member;
	LibFactory factory;
	LibraryFacade facade;
	
	public Library(Member mem, Publication pub){
		this.member = mem;
		this.book = pub;
	}
	
	public LibraryFacade createFacade(){
		//LibraryFacade facade;
		
		facade = factory.createLib(this);
		
		return facade;
		
		
		
	}
	
	public void addMember(String name, MemberType type){
		Member newMem = new Member(name, type);
	}
	
	public void removePub( Publication pub ){
		pub = null;
	}
	
	public void addBook( String title, LateFeesStrategy late){
		Publication newBook = new Book(title, late);
	}
	
	
}
