package edu.ucsd.cse110.library;

public enum MemberType {
	Teacher,
	Student,
	Other
}
